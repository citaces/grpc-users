package storages

import (
	"gitlab.com/citaces/grpc-users/internal/db/adapter"
	"gitlab.com/citaces/grpc-users/internal/infrastructure/cache"
	ustorage "gitlab.com/citaces/grpc-users/internal/modules/user/storage"
)

type Storages struct {
	User ustorage.Userer
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User: ustorage.NewUserStorage(sqlAdapter, cache),
	}
}
