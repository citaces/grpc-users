package router

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/citaces/grpc-users/internal/infrastructure/component"
	"gitlab.com/citaces/grpc-users/internal/infrastructure/middleware"
	"gitlab.com/citaces/grpc-users/internal/modules"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)
			r.Route("/user", func(r chi.Router) {
				userController := controllers.User
				r.Route("/profile", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", userController.Profile)
				})
			})
		})
	})

	return r
}
