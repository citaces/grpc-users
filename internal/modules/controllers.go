package modules

import (
	"gitlab.com/citaces/grpc-users/internal/infrastructure/component"
	ucontroller "gitlab.com/citaces/grpc-users/internal/modules/user/controller"
	"gitlab.com/citaces/grpc-users/internal/modules/user/service"
)

type Controllers struct {
	User ucontroller.Userer
}

func NewControllers(users service.Userer, components *component.Components) *Controllers {

	userController := ucontroller.NewUser(users, components)

	return &Controllers{
		User: userController,
	}
}
