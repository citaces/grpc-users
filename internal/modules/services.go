package modules

import (
	"gitlab.com/citaces/grpc-users/internal/infrastructure/component"
	uservice "gitlab.com/citaces/grpc-users/internal/modules/user/service"
	"gitlab.com/citaces/grpc-users/internal/storages"
)

type Services struct {
	User          uservice.Userer
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
	}
}
