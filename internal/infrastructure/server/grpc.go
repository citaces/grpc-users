package server

import (
	"context"
	"fmt"
	"net"

	"gitlab.com/citaces/grpc-users/config"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

type Server interface {
	Serve(ctx context.Context) error
}

type GRPCserver struct {
	conf   config.RPCServer
	logger *zap.Logger
	srv    *grpc.Server
}

func NewGRPCserver(conf config.RPCServer, srv *grpc.Server, logger *zap.Logger) Server {
	return GRPCserver{conf: conf, logger: logger, srv: srv}
}

func (s GRPCserver) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		var l net.Listener
		l, err = net.Listen("tcp", fmt.Sprintf(":%s", s.conf.Port))
		if err != nil {
			s.logger.Error("grpc server register error", zap.Error(err))
			chErr <- err
		}

		s.logger.Info("grpc server started", zap.String("port", s.conf.Port))

		if err = s.srv.Serve(l); err != nil {
			chErr <- err
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
		s.srv.GracefulStop()
	}

	return err
}
